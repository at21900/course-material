<img title="" src="https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# Lecture 4.1 - UAS Powertrain

[TOC]

## Overview

This module will introduce the following topics:

- ESC input signal

- ESC output signal

- MOSFETs

- Motor control demonstration

- PWM frequency calculations

## Demonstration Setup

### Potentiometer Motor Driver

This demonstration shows the signal used to drive a BLDC motor using an Arduino as the signal generator and a potentiometer as the analog controller. 

#### Setup

**Materials:**

1. BLDC motor

2. Motor propeller mounting hardware

3. Clipped and balanced propeller

4. Motor mounting arm

5. (2) Quick grip clamps (to secure to lectern)

6. Arduino Uno (or similar)

7. 10 KΩ potentiometer

8. (2) 10 KΩ resistors

9. Hookup wires

10. DSO Quad oscilloscope

11. DC power supply (or LiPo)

**Connections:**

![lecture4.1_arduinoDemo](../../img/lecture4.1_arduinoDemo.svg)

1. Install motor on mounting arm

2. Connect ESC to motor (in correct orientation for propeller - CW/CCW)

3. Make the following connections with hookup wires:

| Terminal A       | Connection/Signal            | Terminal B     |
| ---------------- | ---------------------------- | -------------- |
| Servo (Brown)    | ESC servo connector ground   | Arduino (GND)  |
| Servo (Orange)   | ESC servo connector signal   | Arduino (PIN9) |
| Pot (left/right) | Potentiometer terminal (5V)  | Arduino (5V)   |
| Pot (left/right) | Potentiometer terminal (GND) | Arduino (GND)  |
| Pot (wiper)      | Potentiometer wiper output   | Arduino (A0)   |

1. Alligator clip of two probes to single wire on motor

2. 10 KΩ resistor connected to each of the two remaining motor wires (between ESC and motor).

3. Oscope probe connected to each of the two wires through a 10KΩ resistor.

4. Finally, connect the ESC to an appropriate power supply (beware this will energize the system)

**Oscope settings:**

- DC 5V on both probes

- Auto @ 200 µS

- 360 time setting

**Lesson Notes:**

1. Commutation **speed** of the motor is the **width** of the trapezoidal wave
   
   1. i.e. this is how long it takes for the stator and the permanent magnet to pass by one another. 

2. The height of the wave is proportional to the input voltage
   
   1. This can be varied with the power supply.

3. Speed is proportional to voltage (Kv = RPM/V)

4. Adding "load" (propeller) increases the current draw on the ESC.

**Code:**

Download [here](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/week4/lecture4.1/demo4.1_code/demo4.1_code.ino?inline=false)

```cpp
#include <Servo.h>

byte servoPin = 9; // signal pin for the ESC.
byte potentiometerPin = A0; // analog input pin for the potentiometer.
Servo servo;

void setup() {
  Serial.begin(115200);
servo.attach(servoPin);
servo.writeMicroseconds(1500); // send "stop" signal to ESC. Also necessary to arm the ESC.

delay(7000); // delay to allow the ESC to recognize the stopped signal.
}

void loop() {

int potVal = analogRead(potentiometerPin); // read input from potentiometer.
Serial.println(potVal);
int pwmVal = map(potVal,0, 1023, 1300, 1400); // maps potentiometer values to PWM value.

servo.writeMicroseconds(pwmVal); // Send signal to ESC.
}
```

**Lesson Notes:**

This demonstration shows the trapezoidal wave with the overlaid PWM signal that drives the motor and controls the speed.

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
