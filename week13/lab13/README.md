# Lab 13 - Assemble 3D Mount and Install/Wire Peripheral Equipment (30 points)

[TOC]

---

## 1. Objectives

The objective of this lab is to test and install the 3D printed GPS mount and flight controller anti-vibration mount. Once the mount is installed, the peripheral equipment is to be installed and wired.

---

## 2. Overview

In lab 12 we printed a flight controller mount to preemptively mitigate vibrations from motors and propellers as well as a GPS mount to elevate the GPS (and accompanying magnetometer) away from potential interference sources including the wiring and motors. This week we will integrate those prints into the UAS platform. 

We will also install the peripheral equipment and wire it to the flight controller. 

Although we updated/flashed the flight controller firmware in lab 12, it is important to reset the parameters to default as well as verify that the controller has the correct firmware version (i.e. rover, **coper**, plane, etc.).

## 3. 3D Print Installation (5 points)

As you prepare to install the flight controller and GPS mounts, verify the following:

1. Does the print appear correct (tolerances, screw hole alignment, material deficiencies, etc.)?

2. Does the print provide the expected vibration damping?

3. Does the print need any post-processing? 

Using the appropriate hardware\* (screws, nuts, washers, vibration damping material, etc.), install the flight controller mount and GPS mount to your UAS frame. Show the instructor. 

\*be sure to include this hardware specification in your BOM document.

## 4.  Install Flight Controller (5 points)

There are three big considerations for mounting the flight controller:

1. Mount as close as possible to the center of gravity.

2. Mount in line with motors.

3. Mount in a matter to mitigate vibrations.

Review the flight controller mounting information in the [ArduPilot docs](https://ardupilot.org/copter/docs/common-mounting-the-flight-controller.html) and install the flight controller accordingly. You will use [3M® 4466 Double Sided Foam Tape](https://www.officedepot.com/a/products/518956/3M-4466-Double-Sided-Foam-Tape/) to affix the flight controller to your 3D printed mount. This tape is semi-permanent, so ensure proper orientation and positioning before adhering to your mount.

## 5. Install and Wire GPS/Compass (5 points)

There are two big considerations for mounting the GPS/compass:

1. Mount with an unobstructed view of the sky (for GPS reception).

2. Mount away from magnetic interference from wires, motors, and iron-containing metals (for compass accuracy).

Review the GPS/compass mounting information in the [ArduPilot docs](https://ardupilot.org/copter/docs/common-installing-3dr-ublox-gps-compass-module.html#mounting-the-gps-module) and install the GPS unit accordingly using [3M® 4466 Double Sided Foam Tape](https://www.officedepot.com/a/products/518956/3M-4466-Double-Sided-Foam-Tape/). Before adhering to your mount, answer the following question:

1. How should the GPS be oriented? `        `

## 6. Install and Wire Power Module (5 points)

The power module operates between the battery and the UAS power distribution board. Position, mount, and wire the power module according to the [ArduPilot docs](https://ardupilot.org/copter/docs/common-3dr-power-module.html#connecting-to-the-autopilot). Answer the following questions:

1. What is the highest cell count battery that the standard power module can accept? `        `

2. What voltage does the power module supply to the flight controller? `        `

## 7. Wire ESCs (5 points)

The order of the ESC wiring is critical to proper flight operation. Review the [ArduPilot docs](https://ardupilot.org/copter/docs/connect-escs-and-motors.html#connect-escs-and-motors) on wiring the ESCs/motors. Wire the ESC servo/control wires to the flight controller in the proper orientation such that motor 1 of the quad configuration plugs into main out 1 on the flight controller. 

## 8. Install and Wire Transmitter/Receiver (5 points)

The transmitter system will be discussed in more detail in lecture. In the meantime, work with the instructor to wire the receiver to the drone connecting i-Bus (servo) on the receiver to RC IN on the PixHawk. 

## 9. Documentation Update

Be sure to document all installation steps and additional hardware. Begin to document operational procedures for your UAS. Refer to [Lab 6](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week6/lab6/README.md) for documentation information.

## 10.Turn in

Upload completed lab question to your team folder/repository. 

---

:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab11]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
